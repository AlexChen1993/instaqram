/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component, } from 'react';
import { View, Text, Button } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import MainScreen from './components/Main';
import AddScreen from './components/main/Add';
import SaveScreen from './components/main/Save';
import LandingScreen from './components/auth/Landing';
import RegisterScreen from './components/auth/Register';
import LoginScreen from './components/auth/Login';
import CommentScreen from './components/main/Comment';

import firebase from '@react-native-firebase/app';
import auth from '@react-native-firebase/auth';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './redux/reducers';
import thunk from 'redux-thunk';

const store = createStore(rootReducer, applyMiddleware(thunk))

const firebaseConfig = {
  apiKey: "AIzaSyA9AEFdW3iwgLPcaFrZy_0FmGa_JlWiY6c",
  authDomain: "instaqram-36358.firebaseapp.com",
  projectId: "instaqram-36358",
  storageBucket: "instaqram-36358.appspot.com",
  messagingSenderId: "374059059978",
  appId: "1:374059059978:web:cd847b3950a038056ec8f3",
  measurementId: "G-E775PFC0E6"
};

const config = {
    name: 'instaqram',
};

if(firebase.apps.length === 0) {
    firebase.initializeApp(firebaseConfig, config);
    console.log('successfully init firebase')
}

const Stack = createStackNavigator();
export class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            loggedIn: false,
        }
    }
    componentDidMount(){ 
        auth().onAuthStateChanged((user) =>{
            console.log(user);
            if(!user) {
                this.setState({
                    loggedIn: false,
                    loaded: true,
                })
            } else {
                this.setState({
                    loggedIn: true,
                    loaded: true,
                })
            }
        })
    }
    render(){
        const { loggedIn, loaded } = this.state;
        if(!loaded) {
            return(
                <View style={{ flex: 1, justifyContent:'center'}}>
                    <Text>Loading</Text>
                </View>
            )
        }
        if(!loggedIn) {
            return (
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="Landing">
                        <Stack.Screen name="Landing" component={LandingScreen} options={{ headerShown: false }}/>
                        <Stack.Screen name="Register" component={RegisterScreen} options={{ headerShown: false }}/>
                        <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }}/>
                    </Stack.Navigator>
                </NavigationContainer>
            );
        }
        return(
            <Provider store={store}>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="Main">
                    <Stack.Screen name="Main" component={MainScreen}/>
                    <Stack.Screen name="Add" component={AddScreen} navigation={this.props.navigation}/>
                    <Stack.Screen name="Save" component={SaveScreen} navigation={this.props.navigation}/>
                    <Stack.Screen name="Comment" component={CommentScreen} navigation={this.props.navigation}/>
                    </Stack.Navigator>
                </NavigationContainer>
            </Provider>
        )
        
    }
};

export default App;

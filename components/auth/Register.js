import React, { Component } from 'react';
import { View, Button, TextInput } from 'react-native'
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email : '',
            password: '',
            name: '',
        }
        
        this.onSignUp = this.onSignUp.bind(this);
    }

    onSignUp(){
        const { email, password, name } = this.state;
        auth().createUserWithEmailAndPassword(email, password)
        .then((result) => {
            console.log("create successfully")
            firestore().collection('users').doc(auth().currentUser.uid)
            .set({
                name,
                email
            }).then((res)=>{
                console.log('set collection successfully')
                console.log(res)
            })
            .catch((err)=>{
                console.log('set collection failed')
                console.log(err)})
            console.log(result)
        })
        .catch((error) => {
            // Handle Errors here.
            const errorCode = error.code;
            const errorMessage = error.message;
            if (errorCode == 'auth/weak-password') {
                console.log('The password is too weak.');
            } else {
                console.log(errorMessage);
            }
            console.log(error);
        })
    }

    render() {
        return (
            <View>
                <TextInput 
                    placeholder="name"
                    onChangeText={(name) => this.setState({ name })}
                />
                <TextInput 
                    placeholder="email"
                    onChangeText={(email) => this.setState({ email })}
                />
                <TextInput 
                    placeholder="password"
                    onChangeText={(password) => this.setState({ password })}
                />
                <Button
                    onPress={() => this.onSignUp()}
                    title="Sign up"
                />
            </View>
        )
    }
}
export default Register

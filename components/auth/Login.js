import React, { Component } from 'react';
import { View, Button, TextInput } from 'react-native'
import auth from '@react-native-firebase/auth';

export class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email : '',
            password: '',
            name: '',
        }
        
        this.onSignIn = this.onSignIn.bind(this);
    }

    onSignIn(){
        const { email, password, name } = this.state;
        console.log(email);
        console.log(password);
        auth().signInWithEmailAndPassword(email, password)
        .then((result) => {
            console.log(result)
        })
        .catch((error) => {
            // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
            console.log('The password is too weak.');
        } else {
            console.log(errorMessage);
        }
        console.log(error);
        })
    }

    render() {
        return (
            <View>
                <TextInput 
                    placeholder="email"
                    onChangeText={(email) => this.setState({ email })}
                />
                <TextInput 
                    placeholder="password"
                    onChangeText={(password) => this.setState({ password })}
                />
                <Button
                    onPress={() => this.onSignIn()}
                    title="Sign in"
                />
            </View>
        )
    }
}
export default Login

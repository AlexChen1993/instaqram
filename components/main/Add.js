import React, {useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image, Button } from 'react-native';
import { RNCamera } from 'react-native-camera';
import * as ImagePicker from 'react-native-image-picker';

export default function Add({ navigation }) {
    const [camera, setCamera] = useState(null);
    const [image, setImage] = useState(null);
    const [type, setType] = useState(RNCamera.Constants.Type.back);
    const options = {
        storageOptions: {
            skipBackup: true,
            path: 'images',
        },
    };
    const takePicture = async () => {
        if (camera) {
            const options = { quality: 0.5, base64: true };
            const data = await camera.takePictureAsync(options);
            console.log(data.uri);
            setImage(data.uri)
        }
    };

    const pickImage = async () => {
        ImagePicker.launchImageLibrary(options, (response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                setImage(response.assets[0].uri);
                console.log(response.assets[0].uri);
            }
        });
    }

    return (
        <View style={{flex: 1}}>
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        setCamera(ref)
                    }}
                    style={styles.fixedRAtio}
                    type={type}
                    ratio={'1:1'}
                    flashMode={RNCamera.Constants.FlashMode.on}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                />
            </View>
            <View style={{flexDirection: 'row'}}>
                <TouchableOpacity 
                    onPress={() => {
                        setType(
                            type === RNCamera.Constants.Type.back
                            ? RNCamera.Constants.Type.front
                            : RNCamera.Constants.Type.back
                        );
                    }} 
                    style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> Flip Image </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => takePicture() } style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> SNAP </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => pickImage() } 
                    style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> Pick Image </Text>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => navigation.navigate('Save', {image}) } 
                    style={styles.capture}>
                    <Text style={{ fontSize: 14 }}> Save </Text>
                </TouchableOpacity>
            </View>
            <View style={{flex: 1}}>
                {image && <Image source={{uri: image}} style={{flex: 1}}/>}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'black',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 3,
    },
    fixedRAtio: {
        flex: 1,
        aspectRatio: 1,
    }
});


import React, {useState } from 'react';
import { StyleSheet, View, TextInput, Image, TouchableOpacity, Text } from 'react-native';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export default function Save(props, {navigation}) {
    const [caption, setCaption] = useState("")

    const uploadImage = async () => {
        const uri = props.route.params.image;
        const childPath = `post/${auth().currentUser.uid}/${Math.random().toString(36)}`;
        const response = await fetch(uri);
        const blob = await response.blob();

        // uploads file
        const task = storage()
            .ref()
            .child(childPath)
            .put(blob)

        const taskProgress = snapshot => {
            console.log(`transferred: ${snapshot.bytesTransferred}`)
        }

        const taskCompleted = () => {
            task.snapshot.ref.getDownloadURL().then((snapshot) => {
                savePostData(snapshot);
                console.log(snapshot);
            })
        }

        const taskError = snapshot => {
            console.log(snapshot)
        }

        task.on("state_changed", taskProgress, taskError, taskCompleted);
    }

    const savePostData = (downloadURL) => {
        firestore()
            .collection('posts')
            .doc(auth().currentUser.uid)
            .collection("userPosts")
            .add({
                downloadURL,
                caption,
                likeCount: 0,
                creation: firestore.FieldValue.serverTimestamp()
            }).then((function() {
                props.navigation.popToTop()
            }))
    }
    return (
        <View style={{flex: 1}}>
            <Image source={{uri: props.route.params.image}}/>
            <TextInput
                placeholder="Write a Caption ..."
                onChangeText={(caption) => setCaption(caption)}
            />
            <TouchableOpacity 
                onPress={() => uploadImage() } 
                style={styles.capture}>
                <Text style={{ fontSize: 14 }}> Save </Text>
            </TouchableOpacity>
        </View>
    )
}


const styles = StyleSheet.create({
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 3,
    },
});
import React, {useState, useEffect} from 'react'
import { StyleSheet, View, Text, Image, FlatList, TouchableOpacity } from 'react-native';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import { connect } from "react-redux";

function Profile(props) {
    const [userPosts, setUserPosts] = useState([]);
    const [user, setUser] = useState(null);
    const [following, setFollowing] = useState (false);

    const {currentUser, posts} = props;
    useEffect(() => {
        if(props.route.params.uid === auth().currentUser.uid) {
            setUser(currentUser)
            setUserPosts(posts)
        } else {
            firestore()
            .collection("users")
            .doc(props.route.params.uid)
            .get()
            .then((snapshot) => {
                if(snapshot.exists) {
                    setUser(snapshot.data());
                } else {
                    console.log('does not exist');
                }
            })
            firestore()
            .collection("posts")
            .doc(props.route.params.uid)
            .collection("userPosts")
            .orderBy("creation", "asc")
            .get()
            .then((snapshot) => {
                let posts = snapshot.docs.map(doc => {
                    const data = doc.data();
                    const id = doc.id;
                    return { id, ...data};
                })
                setUserPosts(posts)
            })
        }

        if(props.following.indexOf(props.route.params.uid) > -1) {
            setFollowing(true);
        } else {
            setFollowing(false);
        }
    }, [props.route.params.uid, props.following])

    
    console.log({currentUser, posts})
    const onfollow = () => {
        firestore()
            .collection("following")
            .doc(auth().currentUser.uid)
            .collection("userFollowing")
            .doc(props.route.params.uid)
            .set({})
    }

    const onUnfollow = () => {
        firestore()
            .collection("following")
            .doc(auth().currentUser.uid)
            .collection("userFollowing")
            .doc(props.route.params.uid)
            .delete()
    }

    const onLogout = () => {
        auth().signOut();
    }

    if(user === null) {
        return <View/>
    }
    return (
        <View style={styles.container}>
            <View style={styles.containerInfo}>
                <Text>{user.name}</Text>
                <Text>{user.email}</Text>

                {props.route.params.uid !== auth().currentUser.uid ? (
                    <View>
                        {following ? (
                            <TouchableOpacity
                                style = {styles.containerButton}
                                onPress={() => onUnfollow()}
                            >
                                <Text>Following</Text>
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                style = {styles.containerButton}
                                onPress={() => onfollow()}
                            >
                                <Text>Follow</Text>
                            </TouchableOpacity>
                        )
                        }
                    </View>
                ) :
                    <TouchableOpacity
                        style = {styles.containerButton}
                        onPress={() => onLogout()}
                    >
                        <Text>Log out</Text>
                    </TouchableOpacity>}
            </View>

            <View style={styles.containerGallery}>
               <FlatList
                    numColumns={3}
                    horizontal={false}
                    data={userPosts}
                    renderItem={({item}) => (
                        <View style={styles.containerImage}>
                            <Image
                                style={styles.image}
                                source={{uri: item.downloadURL}}
                            />
                        </View>
                    )}
               />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerInfo: {
        margin: 20,
    },
    containerGallery: {
        flex: 1,
    },
    containerImage: {
        flex: 1/3
    },
    image: {
        flex: 1,
        aspectRatio: 1/1
    },
    containerButton: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 3,
    }, 
})
const mapStateToProps = (store) => ({
    currentUser: store.userState.currentUser,
    posts: store.userState.posts,
    following: store.userState.following,
})

export default connect(mapStateToProps, null)(Profile);
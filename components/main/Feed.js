import React, {useState, useEffect} from 'react'
import { StyleSheet, View, Text, Image, FlatList, TouchableOpacity } from 'react-native';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import { connect } from "react-redux";

function Feed(props) {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
       if(props.usersFollowingLoaded === props.following.length && props.following.length !== 0) {
            props.feed.sort(function(x,y) {
               return x.creation - y.creation;
           })
           
           setPosts(props.feed);
       }
    }, [props.usersFollowingLoaded, props.feed])

    const onLikePress = (userId, postId) => {
        firestore()
            .collection("posts")
            .doc(userId)
            .collection("userPosts")
            .doc(postId)
            .collection("likes")
            .doc(auth().currentUser.uid)
            .set({})
    }

    const onDislikePress = (userId,postId) => {
        firestore()
            .collection("posts")
            .doc(userId)
            .collection("userPosts")
            .doc(postId)
            .collection("likes")
            .doc(auth().currentUser.uid)
            .delete()
    }


    return (
        <View style={styles.container}>
            <View style={styles.containerGallery}>
               <FlatList
                    numColumns={1}
                    horizontal={false}
                    data={posts}
                    renderItem={({item}) => (
                        <View style={styles.containerImage}>
                            <Text style={styles.container}>{item.user?.name}</Text>
                            <Image
                                style={styles.image}
                                source={{uri: item.downloadURL}}
                            />
                            { item.currentUserLike ? 
                                (
                                    <TouchableOpacity
                                        onPress={() => onDislikePress(item.user.uid, item.id)}
                                    >
                                        <Text>Dislike</Text>
                                    </TouchableOpacity>
                                ) :
                                (
                                    <TouchableOpacity
                                        onPress={() => onLikePress(item.user.uid, item.id)}
                                    >
                                        <Text>Like</Text>
                                    </TouchableOpacity>
                                )
                            }
                            <Text
                                onPress={() => props.navigation.navigate('Comment',
                                {postId: item.id, uid: item.user.uid})
                            }
                            >
                                View Comments...
                            </Text>
                        </View>
                    )}
               />
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerInfo: {
        margin: 20,
    },
    containerGallery: {
        flex: 1,
    },
    containerImage: {
        flex: 1/3
    },
    image: {
        flex: 1,
        aspectRatio: 1/1
    },
    containerButton: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 3,
    }, 
})
const mapStateToProps = (store) => ({
    currentUser: store.userState.currentUser,
    following: store.userState.following,
    feed: store.usersState.feed,
    usersFollowingLoaded: store.usersState.usersFollowingLoaded,
})

export default connect(mapStateToProps, null)(Feed);
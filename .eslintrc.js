module.exports = {
    root: true,
    extends: '@react-native-community',
    env: {
        node: true,
        browser: true,
        es6: true,
        jest: true,
    },
    parser: 'babel-eslint',
    plugins: ['prettier/recommended'],
    rules: {
        'prettier/prettier': [
            'error',
            {
                useTabs: false,
                tabWidth: 4,
            },
        ],
    },
}
